package uet.oop.bomberman.sound;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.Objects;
import javax.sound.sampled.AudioInputStream;

/**
 * Lớp thêm hiệu ứng âm thanh
 */
public class Sound {

    public static void play(String sound) {
        new Thread(() -> {
            try {
                Clip clip = AudioSystem.getClip();
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                        Objects.requireNonNull(Sound.class.getResourceAsStream("/sound/" + sound + ".wav")));
                clip.open(inputStream);
                clip.start();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }).start();
    }

    public static void stop(String sound) {
        new Thread(() -> {
            try {
                Clip clip = AudioSystem.getClip();
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                        Objects.requireNonNull(Sound.class.getResourceAsStream("/sound/" + sound + ".wav")));
                clip.open(inputStream);
                clip.stop();
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }).start();
    }
}
