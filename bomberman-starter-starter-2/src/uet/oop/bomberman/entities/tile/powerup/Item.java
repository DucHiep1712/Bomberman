package uet.oop.bomberman.entities.tile.powerup;

import uet.oop.bomberman.entities.tile.Tile;
import uet.oop.bomberman.graphics.Sprite;

public abstract class Item extends Tile {
	protected int _duration = -1; // thời gian của item, -1 là vô hạn
	protected boolean _active = false;
	protected int _level;

	public Item(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}
}
