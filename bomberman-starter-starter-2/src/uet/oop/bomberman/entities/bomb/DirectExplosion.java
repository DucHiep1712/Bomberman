package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.entities.character.enemy.Enemy;
import uet.oop.bomberman.graphics.Screen;

public class DirectExplosion extends Entity {

	protected Board _board;
	protected int _direction;
	private int _radius;
	protected int xOrigin, yOrigin;
	protected Explosion[] _explosions = new Explosion[0];

	/**
	 * @param x hoành độ bắt đầu của Flame
	 * @param y tung độ bắt đầu của Flame
	 * @param direction là hướng của Flame
	 * @param radius độ dài cực đại của Flame
	 */
	public DirectExplosion(int x, int y, int direction, int radius, Board board) {
		xOrigin = x;
		yOrigin = y;
		_x = x;
		_y = y;
		_direction = direction;
		_radius = radius;
		_board = board;

		createExplosions();
	}

	/**
	 * Tạo các FlameSegment, mỗi segment ứng một đơn vị độ dài
	 */
	private void createExplosions() {
		// tính toán độ dài Flame, tương ứng với số lượng segment
		_explosions = new Explosion[calculatePermitedDistance()];

		// biến last dùng để đánh dấu cho segment cuối cùng
		boolean last;
		
		int x = (int)_x;
		int y = (int)_y;
		for (int i = 0; i < _explosions.length; i++) {
			last = i == _explosions.length - 1;
			
			switch (_direction) {
				case 0: y--; break;
				case 1: x++; break;
				case 2: y++; break;
				case 3: x--; break;
			}
			_explosions[i] = new Explosion(x, y, _direction, last);
		}
	}

	/**
	 * Tính toán độ dài của Flame, nếu gặp vật cản là Brick/Wall, độ dài sẽ bị cắt ngắn
	 */
	private int calculatePermitedDistance() {
		// Thực hiện tính toán độ dài của Flame
		int radius = 0;
		int x = (int)_x;
		int y = (int)_y;

		while(radius < _radius) {
			if (_direction == 0) y--;
			if (_direction == 1) x++;
			if (_direction == 2) y++;
			if (_direction == 3) x--;
			
			Entity a = _board.getEntity(x, y, null);

			// vụ nổ phải ở bên dưới quả bom
			if (a instanceof Bomb) {
				++radius;
			}

			// không thể đi qua
			if (!a.collide(this)) {
				break;
			}
			++radius;
		}
		return radius;
	}
	
	public Explosion explosionAt(int x, int y) {
		for (Explosion flameSegment : _explosions) {
			if (flameSegment.getX() == x && flameSegment.getY() == y)
				return flameSegment;
		}
		return null;
	}

	@Override
	public void update() {}
	
	@Override
	public void render(Screen screen) {
		for (Explosion flameSegment : _explosions) {
			flameSegment.render(screen);
		}
	}

	@Override
	public boolean collide(Entity e) {
		// xử lý va chạm với Bomber, Enemy. Chú ý đối tượng này có vị trí chính là vị trí của Bomb đã nổ
		if (e instanceof Bomber) {
			((Bomber) e).kill();
		}
		if(e instanceof Enemy) {
			((Enemy) e).kill();
		}
		return true;
	}
}
