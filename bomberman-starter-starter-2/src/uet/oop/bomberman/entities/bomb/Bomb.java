package uet.oop.bomberman.entities.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.entities.AnimatedEntity;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.character.Bomber;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.entities.character.Character;
import uet.oop.bomberman.level.Coordinates;
import uet.oop.bomberman.sound.Sound;

/**
 * Lớp xử lý đối tượng Bomb
 */
public class Bomb extends AnimatedEntity {

	protected double _timeToExplode = 120; // 2 seconds - thời gian phát nổ
	public int _timeAfter = 20; // thời gian để nổ
	
	protected Board _board;
	protected DirectExplosion[] _flames;
	protected boolean _exploded = false;
	protected boolean _allowedToPassThrough = true;
	 
	public Bomb(int x, int y, Board board) {
		_x = x;
		_y = y;
		_board = board;
		_sprite = Sprite.bomb;
	}
	
	@Override
	public void update() {
		if (_timeToExplode > 0) {
			_timeToExplode--;
		} else {
			if (!_exploded) {
				explode();
			} else {
				updateFlames();
			}
			
			if (_timeAfter > 0) {
				_timeAfter--;
			} else {
				remove();
			}
		}
		animate();
	}
	
	@Override
	public void render(Screen screen) {
		if (_exploded) {
			_sprite =  Sprite.bomb_exploded2;
			renderFlames(screen);
		} else {
			_sprite = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, _animate, 60);
		}
		int xt = (int)_x << 4;
		int yt = (int)_y << 4;
		
		screen.renderEntity(xt, yt , this);
	}
	
	public void renderFlames(Screen screen) {
		for (DirectExplosion flame : _flames) {
			flame.render(screen);
		}
	}
	
	public void updateFlames() {
		for (DirectExplosion flame : _flames) {
			flame.update();
		}
	}

    /**
     * Xử lý Bomb nổ
     */
	protected void explode() { // nổ
		_exploded = true;
		_allowedToPassThrough = true;

		// xử lý khi Character đứng tại vị trí Bomb
		Character x = _board.getCharacterAtExcluding((int)_x, (int)_y, null);
		if (x != null) {
			x.kill();
		}

		// tạo các Flame
		_flames = new DirectExplosion[4];
		for (int i = 0; i < _flames.length; i++) {
			_flames[i] = new DirectExplosion((int) _x, (int) _y, i, Game.getBombRadius(), _board);
		}

		Sound.play("BOM_11_M");
	}

	public void time_explode() {
		_timeToExplode = 0;
	}

	public Explosion flameAt(int x, int y) {
		if (!_exploded) return null;

		for (DirectExplosion flame : _flames) {
			if (flame == null) return null;
			Explosion e = flame.explosionAt(x, y);
			if (e != null) return e;
		}
		return null;
	}

	@Override
	public boolean collide(Entity e) {
        // xử lý khi Bomber đi ra sau khi vừa đặt Bomb (_allowedToPassThrough)
        if (e instanceof Bomber) {
			double diffX = e.getX() - Coordinates.tileToPixel(getX());
			double diffY = e.getY() - Coordinates.tileToPixel(getY());

			// check xem liệu người chơi có di chuyển ra khỏi Bomb hay không
			if (!(diffX >= -10 && diffX < 16 && diffY >= 1 && diffY <= 28)) {
				_allowedToPassThrough = false;
			}
			return _allowedToPassThrough;
		}

	// xử lý va chạm với Flame của Bomb khác
		if (e instanceof DirectExplosion) {
			time_explode();
			return true;
		}
		return false;
	}
}
