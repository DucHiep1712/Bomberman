package uet.oop.bomberman.entities;

/**
 * Entity có hiệu ứng hoạt ảnh
 */
public abstract class AnimatedEntity extends Entity {

	protected int _animate = 0;
	protected final int MAX_ANIMATE = 7500; // lưu trạng thái hoạt ảnh
	
	protected void animate() {
		if (_animate < MAX_ANIMATE) {
			_animate++;
		} else {
			_animate = 0; // đặt lại hoạt ảnh
		}
	}
}
